package com.hendisantika.springbootlivestreaming.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-live-streaming
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/01/20
 * Time: 07.26
 */
@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.hendisantika.springbootlivestreaming.domain"})
@EnableJpaRepositories(basePackages = {"com.hendisantika.springbootlivestreaming.repository"})
@EnableTransactionManagement
public class RepositoryConfiguration {

}