package com.hendisantika.springbootlivestreaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootLiveStreamingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootLiveStreamingApplication.class, args);
    }

}
