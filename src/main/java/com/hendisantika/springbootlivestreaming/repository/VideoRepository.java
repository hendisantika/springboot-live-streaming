package com.hendisantika.springbootlivestreaming.repository;

import com.hendisantika.springbootlivestreaming.domain.Video;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-live-streaming
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/01/20
 * Time: 20.18
 */
public interface VideoRepository extends PagingAndSortingRepository<Video, Long> {
    List<Video> findByUserUsername(String user);
}